<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request){
        // Validate
        $validate = $request->validate([
            'name' => 'required|max:100',
            'email' => 'required|unique:users|max:100',
            'password' => 'required',
        ]);

        // password encrypt
        $validate['password'] = Hash::make($validate['password']);
        $user  = User::create($validate);
        // $token = $user->createToken('auth_token');
        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'token' => $token,
            'user' => $user,
        ],201);

    }

    public function logout(Request $request){
        $request->user()->currentAccessToken()->delete();
        return response()->json([
            'message' => 'logout success'
        ],200);
    }

    public function login(Request $request){

        $validated = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = User::where('email',$validated['email'])->first();
        
        if (!$user) {
            return response()->json([
                'message' => 'User not found'
            ],401);
        }

        if (!Hash::check($validated['password'],$user->password)) {
            return response()->json([
                'messsage' => 'Bad credentials'
            ],401);
        }
        
        // if (!$user || !Hash::check($validated['password'],$user->password)) {
        //     return response()->json([
        //         'messsage' => 'Bad credentials'
        //     ],401);
        // }

        $token = $user->createToken('auth_token')->plainTextToken;
        
        return response()->json([
            'token' => $token,
            'user' => $user,
        ],200 );
    }
}
